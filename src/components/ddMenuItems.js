export const menuItems = [
    {
        title: 'Home',
    },
    {
        title: 'About',
    },
    {
        title: "Galleries",
        submenu: [
            {
                title: "2021",
                submenu: [
                    {
                        title: "January",
                    },
                    {
                        title: "February",
                    },
                    {
                        title: "March",
                    },
                    {
                        title: "April",
                    },
                    {
                        title: "May",
                    },
                    {
                        title: "June",
                    },
                    {
                        title: "August",
                    },
                    {
                        title: "September",
                    },
                    {
                        title: "October",
                    },
                    {
                        title: "November",
                    },
                    {
                        title: "December",
                    },
                ],
            },
            {
                title: "2022",
                submenu: [
                    {
                        title: "January",
                    },
                    {
                        title: "February",
                    },
                ],
            },
        ],
    },
    {
        title: 'Contact',
    },
    {
        title: 'In-Bloom Uploader',
    },
];